Source: libbio-db-biofetch-perl
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Michael R. Crusoe <crusoe@debian.org>
Section: perl
Priority: optional
Build-Depends: debhelper-compat (= 12),
               libbio-perl-perl,
               libhttp-message-perl,
               libtest-exception-perl,
               libtest-requiresinternet-perl,
               libtest-warn-perl,
               libwww-perl
Rules-Requires-Root: no
Build-Depends-Indep: perl
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/med-team/libbio-db-biofetch-perl
Vcs-Git: https://salsa.debian.org/med-team/libbio-db-biofetch-perl.git
Homepage: https://metacpan.org/release/Bio-DB-BioFetch

Package: libbio-db-biofetch-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libbio-perl-perl,
         libhttp-message-perl,
         libwww-perl
Breaks: libbio-perl-perl (<< 1.7.3)
Replaces: libbio-perl-perl (<< 1.7.3)
Description: Database object interface to BioFetch retrieval
 Bio::DB::BioFetch is a guaranteed best effort sequence entry fetching method.
 It goes to the Web-based dbfetch server located at the EBI
 (http://www.ebi.ac.uk/Tools/dbfetch/dbfetch) to retrieve sequences in the
 EMBL or GenBank sequence repositories.
 .
 Bio::DB::BioFetch implements all the Bio::DB::RandomAccessI interface, plus
 the get_Stream_by_id() and get_Stream_by_acc() methods that are found in the
 Bio::DB::SwissProt interface.
